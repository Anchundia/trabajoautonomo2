package edu.uoc.android.restservice.ui.enter;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.adapter.GitHubAdapter;
import edu.uoc.android.restservice.rest.model.Follower;
import edu.uoc.android.restservice.rest.model.Owner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.Thread.sleep;


public class InfoUserActivity extends AppCompatActivity {

    ArrayList<Owner> listaFollowers;
    RecyclerView recyclerViewFollowers;

    //muestra los datos en el recyclerview
    RecyclerView.Adapter adapter_;

    /* se guarda el contenido del xml*/
    View conten;

    // se declaran las variables variables
    ProgressBar progressBar;
    TextView textViewRepositories, textViewFollowing;
    ImageView imageViewProfile;
    TextView error;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuarios);

        // se indica los componentes
        conten = findViewById(R.id.content);
        progressBar = findViewById(R.id.progress);
        textViewFollowing = findViewById(R.id.textViewFollowing);
        textViewRepositories = findViewById(R.id.textViewRepositories);
        imageViewProfile = (ImageView) findViewById(R.id.imageViewProfile);
        recyclerViewFollowers = (RecyclerView)findViewById(R.id.recyclerViewFollowers);
        recyclerViewFollowers.setLayoutManager(new LinearLayoutManager(this));
        error = (TextView) findViewById(R.id.error);

        // se crea una lista que contiene a los seguidores
        listaFollowers = new ArrayList<>();

        //  variable para guardar el nombre por buscar
        String loginName;
        // recibe datos de una actividad a otra
        Bundle extras = getIntent().getExtras();
        // si es nulo
        if(extras == null) {
            // se manda este mensaje de pantalla
        } else {
            // caso contratio se busca la información necesaria del usuario
            loginName= getIntent().getStringExtra("loginName");

            // muestra la cantidad de repositorios, de seguidores y su foto
            mostrarDatosBasicos(loginName);

        }
    }

    // muestra la cantidad de repositorios, de seguidores y su foto
    private void mostrarDatosBasicos( final String loginName){
        showLoading();
        // constuctor hacia la clase GitHubAdapter
        GitHubAdapter adapter = new GitHubAdapter();

        // se establec una paeticion segun el nombre del usuario
        Call<Owner> call = adapter.getOwner(loginName);
        call.enqueue(new Callback<Owner>() {

            @Override
            public void onResponse(Call<Owner> call, Response<Owner> response) {
                // se guarda en una variable tipo Owner el cupero de la respuesta
                Owner owner = response.body();

                if (owner!=null){
                    // Se envia la cantidad de repositorios y seguidores, y la imagen del usuario
                    textViewRepositories.setText(owner.getPublicRepos().toString());
                    textViewFollowing.setText(owner.getFollowers().toString());
                    Picasso.with(getApplicationContext()).load(owner.getAvatarUrl()).into(imageViewProfile);
                    // muestra una lista de los seguidores del usuario
                    mostrarFollowers(loginName);
                }else{
                    //Alerta
                    showError();
                    /*Toast.makeText(getApplicationContext(),"No existe este usuario", Toast.LENGTH_LONG).show();
                    onBackPressed();*/
                }
            }

            @Override
            public void onFailure(Call<Owner> call, Throwable t) {
                //Alerta
                showError();
            }
        });
    }

    // muestra una lista de los seguidores del usuario
    private void mostrarFollowers(String loginName){
        final ArrayList<Follower> seguidores = new ArrayList<>();

        GitHubAdapter adapter = new GitHubAdapter();

        Call<List<Owner>> call = adapter.getOwnerFollowers(loginName);
        call.enqueue(new Callback<List<Owner>>() {
            @Override
            public void onResponse(Call<List<Owner>> call, Response<List<Owner>> response) {
                List<Owner> list = response.body();
                if (list!=null){
                    for (Owner a : list){
                        seguidores.add(new Follower(a.getLogin(),a.getAvatarUrl()));
                    }
                    Log.i("Lista",seguidores.toString());
                    adapter_ = new AdaptadorFollowers(seguidores);
                    adapter_.notifyDataSetChanged();
                    recyclerViewFollowers.setAdapter(adapter_);
                    showUserContent();
                }else{
                    showError();}
            }

            @Override
            public void onFailure(Call<List<Owner>> call, Throwable t) {
                showError();
            }
        });
    }

    // muestra el progress
    private void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setMax(100);
        conten.setVisibility(View.GONE);
        error.setVisibility(View.GONE);
    }

    // muestra el mensaje de error
    private void showError() {
        progressBar.setVisibility(View.GONE);
        conten.setVisibility(View.GONE);
        error.setVisibility(View.VISIBLE);
    }

    // muestra los seguidores del usuario
    private void showUserContent() {
        progressBar.setVisibility(View.GONE);
        conten.setVisibility(View.VISIBLE);
        error.setVisibility(View.GONE);
    }
}
