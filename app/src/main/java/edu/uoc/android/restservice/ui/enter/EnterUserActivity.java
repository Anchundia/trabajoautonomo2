package edu.uoc.android.restservice.ui.enter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import edu.uoc.android.restservice.R;

public class EnterUserActivity extends AppCompatActivity {

    //se declaran las varibales de busqueda
    private EditText etUser;
    private Button btnFollowers;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_user);
        initViews();
    }

    private void initViews() {
        etUser = findViewById(R.id.enter_user_edit_text);
        btnFollowers = findViewById(R.id.enter_user_button);
        btnFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validar();
            }
        });
    }

    //Metodo para validar que el input no este vacio
    public void validar(){
        etUser.setError(null);

        String usuario = etUser.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(usuario)) {
            etUser.setError(getString(R.string.mensaje));
            focusView = etUser;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            progressDialog();
        }
    }

    public void progressDialog(){
        //muestra la información del cliente
        Intent i = new Intent(EnterUserActivity.this, InfoUserActivity.class);
        i.putExtra("loginName", etUser.getText().toString());
        startActivity(i);
    }
}
